const express = require("express");
// The "taskController" allows us to use the function defined inside it.
const taskController = require ("../controllers/taskController");

// Allows access to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router();

// Route to GET all tasks
router.get("/", (req, res)=>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to CREATE a Task
// localhost:3001/task -is the same with localhost:3001/task/
router.post("/", (req, res)=>{
	// The ".createTask" function needs data from the request body, so we need it to supply in the taskController.creaTask(argument).
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to DELETE a task
// colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// ":id" is a wildcard where you can put the objectID as the value.
// ex: localhost:3001/tasks/:id or localhost:3001/tasks/123456
router.delete("/:id", (req, res) =>{
	// If information will be coming from the URL, the data can be accessed from the request "params" property
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to UPDATE a task
router.patch("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Route to GET a specific task
router.get("/:id", (req, res)=>{

	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to CHANGE/UPDATE the status of a task to "complete"
router.put("/:id/complete", (req, res) =>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Use "module.exports" to export the router object to be use in the server
module.exports = router;

