// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//  This allow us to use ALL the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoutes");

// Server Setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// MongoDB Connection String
mongoose.connect("mongodb+srv://admin:admin@course-booking.oluqr.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set the notification for connection or failure
let db = mongoose.connection;

// Notify on error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

// Add the task route
// Allows ALL the task routes created in the "taskRoutes.js" file to use "/tasks" route
// localhost:3001/tasks
app.use("/tasks", taskRoute);


// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

/*
	Separation of Concerns

	Model Folder
		- Contains the object schemas and defines the object structure and content

	Controllers Folder
		- Contains the function and business logic of our JS application
		- Meaning all the operations it can do will be placed here.

	Routes folder
		- Contains all the endpoints and assign the http methods for our application. i.e. app.get("/", )
		- We separate the routes such that the server/"index.js" only contains information on the server.

	JS modules
		require => -to include a specific module/package.
		module.exports => -to treat a value as a "package" that can be used by other files.

	Flow of exports and require:
	export models > require in controllers
	export controllers > requires in routes
	expert routes > require in server (index.js)

*/






